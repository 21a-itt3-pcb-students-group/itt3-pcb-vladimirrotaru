Worklog 23/08/21 *week 34*
* Summary of activities (What did you plan to do and what did you actually do)
	For today's lessons I have planned to do a research on which circuit I should choose for my future personal project. I will try to absorb any useful information which will aid me into circuit development and building. Also I will set up an gitlab project where I will store all of my progress along with a lot of personal notes which would aid me in the future sessions. OrCad installation and license might not be possible to solve current week as I am currently awaiting for the rehost license answer from the Nordcad support team. 


* Lessons learned (compared to the learning goals)
Learning goals for the current week:

"Circuit research (LGK5, LGS2, LGC3, LGC5)
Level 1: The student knows details and complexity in the chosen circuit - Done
Level 2: The student can validate different circuits and validate them according to complexity and the students skill level - Done
Level 3: The studnet can aquire new knowledge about the circuit that they are going to design a PCB from - Done

Setup gitlab documentation project + project log (LGK1, LGC2)
Level 1: The student can set up a gitlab project correctly - Done
Level 2: The student can manage own tasks in the gitlab project - Done
Level 3: The student can document their own work and project progress - Done

OrCad installation and license (LGS1)
Level 1: The student can download and install the OrCad suite - Done
Level 2: The student can aquire a student license from Nordcad - Done
Level 3: The student can activate the license" - Done

1. Research and decide on the circuit you want to design and build
I have decided to pick a circuit from the circuit catalogue - RPi IoT hat
The hardware attached on top(hat) will contain different sensors which will mark my system as individual.
A security system built in a hat, which consists of an RFID reader which, when scanned with a card or chip, will display the current value of the chip to a small dimension screen. The user will have the posibility to change the value seen on the screen using two buttons where one would have the role of incrementor and the other the role of decrementor. The HAT will also have 2 LED's which will have an indivitual role. One of them will be lit on whenever the system will be running, while the other one will light on at the moment the card or chip is being scanned by the RFID reader.
 * Change amount x on the card/chip to another amount y
More thought have to be put into this.
2. Create a block diagram with logical subcircuits and specifications for input and output values between circuits.
Can be found in diagrams folder.
3. Document with a schematic drawn in Orcad capture.
Schematic can be found in schematics folder.
4. If needed, identify and document test points and their values.
In this document all of the documentation can be found.

Notes:
In the case of designing a new add-on board, which takes advantage of the pins on the 40W GPIO header other than the original 26 then there are some basic requirements that have to be followed:
1. The ID_SC and ID_SD pins must only be used for attaching a compatible ID EEPROM. *Do not use ID_SC and ID_SD pins for anything except connecting an ID EEPROM, if unused these pins must be left unconnected*
2. If back-powering via the 5V GPIO header pins, you must make sure that it is safe to do so even if the 5V supply is also connected. Adding an ideal 'safety' diode as per the relevant section of the design guide is the recommented way to do this.
From this I understand that a 'safety' diode should be used in order to prevent any possible short-circuits because of back-powering.
Back-powering(definition): Back Powering is what happens when data entering an un-powered device's input pin is routed up through the internal ESD diodes in the device onto its power rail. This often provides enough power to run the device (escpecially with idle-high signals coming in) but with (often dire) consequences.
3. The board must protect against old firmware accidentally driving GPIO6,14,16 at boot time if any of those pins are also driven by the board itself.
In my opinion GPIO6,14,16 have to be taken into consideration when designing and running a HAT.

Note that for new designs that only use the priginal 26 way GPIO header pins it is still recommended to follow requirement 2. If the board supports back-powering a Pi.

*HAT requirements*
A board can only be called a HAT if:
1. It conforms to the basic add-on board requirements.
2. It has a valid ID EEPROM (including vendor info, GPIO map and valid device tree information)
3. It has a full size 40W GPIO connector.
4. It follows the HAT mechanical specification.
*see HAT board mechanical specification resource for schematic*
5. It uses a GPIO connector that spaces the HAT at least 8mm from the Pi 
(i.e. uses spacers 8mm or larger - also see note on PoE header below)
6. If back powering via the GPIO connector the HAT must be able to supply a minimum of 1.3A continuously to the Pi (but ability to supply 2A continuously is recommended).

Of course users are free to put an ID EEPROM on boards that don't otherwise conform to the remainder of the specifications - in fact we strongly encourage this; we just want things called HATs to be known and well-specified entity to make life easier for customers, particularly the less technical ones.
NOTE that the Pi3B+ introduced a new 4-pin PoE header near the top-right corner mounting hole. Newly designed HATs that do not provide a connector for this header must avoid fouling it.(Not my case)

Design Resources
It is recommended to read the design guide carefully before designing any new add-on board(HAT compliant or not)
For what to flash into the ID EEPROM see the ID EEPROM data format spec.
You can see the EEPROM data format spec in the resources section. (The spec is preliminary and therefore still likely to change)
There are tools and documentation on how to flash ID EEPROMs
In the resources used you can find it under "EEPROM utils to create, flash and dump HAT EEPROM images"

* Resources used ()
1. Exercises week 34 link -
https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww34
2. Circuit catalogue -
https://eal-itt.gitlab.io/21a-itt3-pcb/other-docs/circuit_catalogue.html
3. Standard outlined by Raspberry Pi foundation which has to be followed -
https://github.com/raspberrypi/hats
4. Raspberry Pi design guide -
https://github.com/raspberrypi/hats/blob/master/designguide.md
5. Back-powering definition & info - 
https://electronics.stackexchange.com/questions/134157/jtag-circuit-to-prevent-back-powering#:~:text=Back%20Powering%20is%20what%20happens,with%20(often%20dire)%20consequences.
6. HAT board mechanical specification - 
https://github.com/raspberrypi/hats/blob/master/hat-board-mechanical.pdf
7. EEPROM data format spec - 
https://github.com/raspberrypi/hats/blob/master/eeprom-format.md
8. EEPROM utils to create, flash and dump HAT EEPROM images -
https://github.com/raspberrypi/hats/tree/master/eepromutils

Worklog 30/08/21 *week 35*
* Summary of activities (What did you plan to do and what did you actually do)
During this lesson the first exercise was to complete the beginner courses to Nordcad which are divided in 3 sections: Schematic, Simulation, PCB Design. While working with these beginner courses, courses were documented and uploaded to gitlab. However, I have found some parts of the beginner courses very poorly explained and the video would start with stuff that he has added in advance, which has made me very confused. As I have been surfing on the nordcad webpage, I have found other courses which were more fresh and well explained. I will leave a link below for the ones that I found more challenging and informative.
Schematic has been developed for the circuit and uploaded to the gitlab repository. System has been divded in more subcircuits which have been simulated in order to ensure that everything is working as intended.

* Resources used ()
https://www.nordcad.eu/student-forum/online-courses/

Worklog 6/09/21 *week 36*
* Summary of activities (What did you plan to do and what did you actually do)
During this week a list of tasks have been presented. A lot of research had to be done in order to have a better understanding of what we will be working with. First task required us to investigate different types of SMT component packages like Passive rectangular components, Integrated circuit SMD packages and Trasnistor & diode packages. A spreadsheet has been created and uploaded to gitlab.
* Resources used ()
https://www.electronics-notes.com/articles/electronic_components/surface-mount-technology-smd-smt/packages.php
https://www.pcbonline.com/blog/pcb-smt-components.html
All of the information used in the component research datasheet file has been used from the gitlab datasheet pdf files.
https://www.youtube.com/watch?v=zFTEcsgR8iE&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=5
https://www.youtube.com/watch?v=CYtn_WWPizo&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=7
https://www.youtube.com/watch?v=s8mYUTix3ao
https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx

Worklog 13/09/21 *week 37*
* Summary of activities (What did you plan to do and what did you actually do)
During this session more things have been accomplished towards the steps of designing the pcb, a design rule check has been executed in order to remove the errors before transferring my design to the pcb editor. Schematic DRC will check my design for design rule violations and errors will be placed on my schematic in order to give me an understanding where I should fix them. Next step in order to transfer the schematic and footprints is to generate a netlist. Design parameters and grid spacing has to be specified in order to get ready for PCB design, board outline and 2 layers has been added to the design. Mounting holes have been added according to the HAT mechanical  specification found on the gitlab page below(check resources).

* Resources used ()
https://github.com/raspberrypi/hats/blob/master/hat-board-mechanical.pdf
https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww37
https://resources.orcad.com/orcad-capture-tutorials/orcad-capture-tutorial-09-perform-a-schematic-drc
https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx
https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-02-generate-a-pcb-editor-netlist-in-orcad-capture
https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-03-set-up-pcb-design-and-draw-a-board-outline
https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-04-create-and-place-mechanical-symbols

Worklog 13/09/21 *week 38*
Nordcad Workshop
During this workshop I managed to find my answers to some of the questions that appeared while I was working with the tools provided by them. Also, they have provided us professional courses which helped me so much to have a better understanding of use of tools in capture, and improved my overall capture skills. Nordcad guys were very friendly walking around and assisting everbody with any possible question.

Worklog 20/09/21 *week 39*
Design for manufacturing Workshop
Workshop was conducted by Soren Elsner who is a NPI engineer at Universal Robots. He introduced us to the main concepts in regards to PCB manufacture. 
https://www.mclpcb.com/blog/design-for-manufacturing-pcbs/

Worklog 01/11/21 *week 44*
* Summary of activities (What did you plan to do and what did you actually do)
Board outline has been looked into in order to define the physical outline of my PCB. The outline has been created following the mechanical specification of the Raspberrypi HAT design guidelines. Mounting holes using mechanical symbols exercise taught me how to create and place mechanical symbols on my PCB. Also during this week I have placed my components on my PCB.

List of DFM considerations that I am going to implement in my PCB Design:
Avoid placing components on the solder side of a board.
You should terminate all lands with only one trace.
Solder shorts: Surface mount pins that are very close together may form thin slivers of solder between them. These solder slivers can create intermittent shorts that are very difficult to find and correct.
Mechanical outline: In order to create an accurate board outline model within the CAD system, a detailed mechanical drawing of the shape and size of the board is required. It should also include details on where to place fixed components and other mechanical features like mounting holes and slots. To avoid costly redesigns and assembly errors, good communication between all members of the design team is essential.
Thru-holes that are too large for their pins can disperse solder too quickly through them resulting in a bad solder connection.
PCB Test: In order to verify the accuracy of the assembly process, circuit boards normally include testpoints that are accessible to the probes of a test fixture. If the testpoints aren’t included in the original design however, the board will have to be redesigned to add them before it can graduate to production. Redesigns like this are not only costly and time-consuming, they may also introduce new DFM problems that didn’t exist before. 

* Resources used ()
https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx
https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-03-set-up-pcb-design-and-draw-a-board-outline
https://resources.orcad.com/orcad-pcb-editor-tutorials/orcad-pcb-editor-tutorial-04-create-and-place-mechanical-symbols

Worklog 08/11/21 *week 45*
* Summary of activities (What did you plan to do and what did you actually do)
Routed the Board
Set up the silk screen
Cleared DRC errors
Design sanity check
Created Gerber and drill files
Inspected gerber and drill files
Placed a order on JLCPCB of the pcb

Worklog 15/11/21 *week 46*
During this session, a complete overview of exercises have been added to the gitlab readme file.
Everything has been documented throughoutly

Worklog 22/11/21 *week 47*
During this session we have spent our time on assembling the PCB's, sadly my PCB had a mistake and I was not able to test it out properly. I have redid the schematic and the PCB design and ordered up a new PCB.

Worklog 29/11/21 *week 48*
* Summary of activities (What did you plan to do and what did you actually do)
As my project has been already converted to KiCad because of an error encountered before, I find it very easy working with KiCad. It being an extremely user friendly open source tool. More experience has been gained with KiCad during this session.

Worklog 6/12/21 *week 49*
This week's session has been spent on exclusively writing the report.