EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 61892F00
P 3200 850
F 0 "J2" H 3280 842 50  0000 L CNN
F 1 "OLED" H 3280 751 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 3200 850 50  0001 C CNN
F 3 "~" H 3200 850 50  0001 C CNN
	1    3200 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:Buzzer BZ1
U 1 1 618947DC
P 4550 2100
F 0 "BZ1" H 4702 2129 50  0000 L CNN
F 1 "Buzzer" H 4702 2038 50  0000 L CNN
F 2 "Buzzer_Beeper:Buzzer_12x9.5RM7.6" V 4525 2200 50  0001 C CNN
F 3 "~" V 4525 2200 50  0001 C CNN
	1    4550 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 61894F7D
P 2900 2250
F 0 "D1" H 2893 2467 50  0000 C CNN
F 1 "LED" H 2893 2376 50  0000 C CNN
F 2 "LED_THT:LED_D1.8mm_W3.3mm_H2.4mm" H 2900 2250 50  0001 C CNN
F 3 "~" H 2900 2250 50  0001 C CNN
	1    2900 2250
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D2
U 1 1 61895C6D
P 2900 2650
F 0 "D2" H 2893 2867 50  0000 C CNN
F 1 "LED" H 2893 2776 50  0000 C CNN
F 2 "LED_THT:LED_D1.8mm_W3.3mm_H2.4mm" H 2900 2650 50  0001 C CNN
F 3 "~" H 2900 2650 50  0001 C CNN
	1    2900 2650
	-1   0    0    1   
$EndComp
Text GLabel 1400 800  0    50   Input ~ 0
3.3V
Text GLabel 1400 900  0    50   Input ~ 0
GPIO2(SDA)
Text GLabel 3000 950  0    50   Input ~ 0
GPIO3(SCL)
Text GLabel 1400 1200 0    50   Input ~ 0
Ground
Text GLabel 1400 1400 0    50   Input ~ 0
GPIO27
Text GLabel 1400 1600 0    50   Input ~ 0
3.3V
Text GLabel 1400 1700 0    50   Input ~ 0
GPIO10(MOSI)
Text GLabel 1400 1800 0    50   Input ~ 0
GPIO9(MISO)
Text GLabel 1400 1900 0    50   Input ~ 0
GPIO11(SCLK)
Text GLabel 1400 2000 0    50   Input ~ 0
Ground
Text GLabel 1400 2600 0    50   Input ~ 0
GPIO26
Text GLabel 1400 2700 0    50   Input ~ 0
Ground
Text GLabel 1900 800  2    50   Input ~ 0
5V
Text GLabel 1900 900  2    50   Input ~ 0
5V
Text GLabel 1900 1000 2    50   Input ~ 0
Ground
Text GLabel 1900 1400 2    50   Input ~ 0
Ground
Text GLabel 1900 1500 2    50   Input ~ 0
GPIO23
Text GLabel 1900 1700 2    50   Input ~ 0
Ground
Text GLabel 1900 1800 2    50   Input ~ 0
GPIO25
Text GLabel 1900 1900 2    50   Input ~ 0
GPIO8(CE0)
Text GLabel 1900 2200 2    50   Input ~ 0
Ground
Text GLabel 1900 2400 2    50   Input ~ 0
Ground
Text GLabel 3000 850  0    50   Input ~ 0
3.3V
Text GLabel 3000 750  0    50   Input ~ 0
Ground
Text GLabel 3000 1350 0    50   Input ~ 0
3.3V
Text GLabel 3000 1450 0    50   Input ~ 0
GPIO25
Text GLabel 3000 1550 0    50   Input ~ 0
Ground
Text GLabel 3000 1750 0    50   Input ~ 0
GPIO9(MISO)
Text GLabel 3000 1950 0    50   Input ~ 0
GPIO11(SCLK)
Text GLabel 3000 1850 0    50   Input ~ 0
GPIO10(MOSI)
Text GLabel 3000 2050 0    50   Input ~ 0
GPIO8(CE0)
Text GLabel 4450 2200 0    50   Input ~ 0
Ground
Text GLabel 4450 2000 0    50   Input ~ 0
GPIO23
Text GLabel 4550 850  2    50   Input ~ 0
Ground
Text GLabel 4550 1500 2    50   Input ~ 0
Ground
Text GLabel 3050 2250 2    50   Input ~ 0
Ground
Text GLabel 3050 2650 2    50   Input ~ 0
Ground
Text GLabel 2750 2250 0    50   Input ~ 0
GPIO27
Text GLabel 2750 2650 0    50   Input ~ 0
GPIO26
$Comp
L Switch:SW_Push_Open_Dual SW1
U 1 1 618B0D33
P 4350 850
F 0 "SW1" H 4350 1060 50  0000 C CNN
F 1 "Button" H 4350 969 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4350 1050 50  0001 C CNN
F 3 "~" H 4350 1050 50  0001 C CNN
	1    4350 850 
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push_Open_Dual SW2
U 1 1 618B2476
P 4350 1500
F 0 "SW2" H 4350 1710 50  0000 C CNN
F 1 "Button" H 4350 1619 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4350 1700 50  0001 C CNN
F 3 "~" H 4350 1700 50  0001 C CNN
	1    4350 1500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 619F8512
P 1600 1700
F 0 "J1" H 1650 2817 50  0000 C CNN
F 1 "Rpi4 gpio" H 1650 2726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 1600 1700 50  0001 C CNN
F 3 "~" H 1600 1700 50  0001 C CNN
	1    1600 1700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J3
U 1 1 61AD0FA5
P 3200 1650
F 0 "J3" H 3280 1642 50  0000 L CNN
F 1 "RFID-RC522" H 3280 1551 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Horizontal" H 3200 1650 50  0001 C CNN
F 3 "~" H 3200 1650 50  0001 C CNN
	1    3200 1650
	1    0    0    -1  
$EndComp
Text GLabel 1400 1000 0    50   Input ~ 0
GPIO3(SCL)
Text GLabel 3000 1050 0    50   Input ~ 0
GPIO2(SDA)
NoConn ~ 3000 1650
NoConn ~ 1400 1300
Text GLabel 1900 2500 2    50   Input ~ 0
GPIO16
Text GLabel 1400 2300 0    50   Input ~ 0
GPIO6
Text GLabel 4150 850  0    50   Input ~ 0
GPIO16
Text GLabel 4150 1500 0    50   Input ~ 0
GPIO6
NoConn ~ 4150 1050
NoConn ~ 4550 1050
NoConn ~ 4150 1700
NoConn ~ 4550 1700
NoConn ~ 1900 2700
NoConn ~ 1900 2600
NoConn ~ 1400 2500
NoConn ~ 1400 2400
NoConn ~ 1400 2200
NoConn ~ 1400 2100
NoConn ~ 1900 2100
NoConn ~ 1900 2000
NoConn ~ 1900 2300
NoConn ~ 1900 1600
NoConn ~ 1900 1300
NoConn ~ 1900 1200
NoConn ~ 1900 1100
NoConn ~ 1400 1100
NoConn ~ 1400 1500
$EndSCHEMATC
